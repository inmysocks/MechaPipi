// Trigger input events
var robot = require("robotjs");
// Capture input events
var ioHook = require("iohook");

var server = true;
if (!process.argv[2]) {
  server = true;
} else {
  server = process.argv[2]
  addressThing = process.argv[3]
}

var screenSize = robot.getScreenSize();
var position = {x:-1, y:-1};
var clicks = -1
var key = -1
var message = {position: position, clicks: clicks, key: key}
var connections = [];
// This is the port used by the web socket server
var SERVER_PORT = 8000;
if (server === true) {
  console.log('SERVER')
  startIOHook();
  var WebSocketServer = require('ws').Server;
  // Create the web socket server on the defined port
  wss = new WebSocketServer({port: SERVER_PORT});
  // Initialise the connections array
  //var connections = new Array;
  // Put a 0 in the array to start, it wasn't working without putting something // here for some reason.
  var ip = require("ip");
  var ipAddress = ip.address();
  console.log("Address ", ipAddress, ':', SERVER_PORT);
  var handleConnection = function(client) {
    console.log("new connection");
    connections.push({'socket':client, 'active': true});
    client.on('message', function incoming(event) {
      /*
      // For the fuure when we have multiple computers connected. This should
      // handle setting up each new connection on the server side.
      try {
        var eventData = JSON.parse(event);
        if (typeof messageHandlers[eventData.messageType] === 'function') {
          messageHandlers[eventData.messageType](eventData);
        } else {
          console.log('No handler for message of type ', eventData.messageType);
        }
      } catch (e) {
        console.log(e);
      }
      */
    });
    //position = robot.getMousePos();
    //connections[0].socket.send(JSON.stringify({'x': position.x/screenSize.width, 'y': position.y/screenSize.height}));
  }
  // Set the onconnection function
  wss.on('connection', handleConnection);
} else {
  console.log('CLIENT')
  var openSocket = function () {
    console.log('Opened Socket');
  }

  var parseMessage = function(event) {
    console.log(event.data);
    var button = false;
    var data = JSON.parse(event.data);
    if (data.clicks === 1) {
      button = "left";
    } else if (data.clicks === 2) {
      button = "middle";
    } else if (data.clicks === 3) {
      button = "right";
    }
    if (typeof button === 'string') {
      robot.mouseClick(button);
    }
    if (data.key !== 0 && data.key !== -1) {
      robot.keyTap(getKey(data.key));
      //robot.keyTap(String.fromCharCode(data.key));
    }
    if (data.position.x !== -1) {
      robot.moveMouse(Math.floor(data.position.x*screenSize.width), Math.floor(data.position.y*screenSize.height));
    }
  }

  var WebSocket = require('ws');
  var ws = new WebSocket(`ws://${addressThing}:${SERVER_PORT}`);
  ws.onopen = openSocket;
  ws.onmessage = parseMessage;
  ws.binaryType = "arraybuffer";
}

var twoPI = Math.PI * 2.0;
var height = (screenSize.height / 2) - 10;
var width = screenSize.width;

var processID = false;

if (server === true) {
  var displayPosition = function () {
    if (connections[0]) {
      if (typeof connections[0].socket.send === 'function') {
        message = {position: position, clicks: clicks, key: key}
        connections[0].socket.send(JSON.stringify(message));
        clicks = 0;
        key = 0;
      }
    }
  }
  processID = setInterval(displayPosition, 20)
} else {
  setInterval(function(){console.log('thing')},1000)
}

function startIOHook () {
  ioHook.on("mousedown",function(msg){
    console.log(msg);
    clicks = msg.button;
  });
  ioHook.on("keypress",function(msg){
    console.log(msg);
    key = msg.keychar;
  });
  // ioHook.on("keydown",function(msg){console.log(msg);});
  // ioHook.on("keyup",function(msg){console.log(msg);});
  // ioHook.on("mouseclick",function(msg){console.log(msg)});
  // ioHook.on("mousewheel",function(msg){console.log(msg)});
  // ioHook.on("mousemove",function(msg){console.log(msg)});
  ioHook.on("mousedrag",function(msg){
    console.log(msg)
    position = {'x': msg.x/screenSize.width, 'y': msg.y/screenSize.height};
  });
  //start ioHook
  ioHook.start();
}

function getKey (code) {
  switch (code) {
    case 32: return " ";
    case 33: return "!";
    case 34: return "\"";
    case 35: return "#";
    case 36: return "$";
    case 37: return "%";
    case 38: return "&";
    case 39: return "'";
    case 40: return "(";
    case 41: return ")";
    case 42: return "*";
    case 43: return "+";
    case 44: return ",";
    case 45: return "-";
    case 46: return ".";
    case 47: return "/";
    case 48: return "0";
    case 49: return "1";
    case 50: return "2";
    case 51: return "3";
    case 52: return "4";
    case 53: return "5";
    case 54: return "6";
    case 55: return "7";
    case 56: return "8";
    case 57: return "9";
    case 58: return ":";
    case 59: return ";";
    case 60: return "<";
    case 61: return "=";
    case 62: return ">";
    case 63: return "?";
    case 64: return "@";
    case 65: return 'A';
    case 66: return 'B';
    case 67: return 'C';
    case 68: return 'D';
    case 69: return 'E';
    case 70: return 'F';
    case 71: return 'G';
    case 72: return 'H';
    case 73: return 'I';
    case 74: return 'J';
    case 75: return 'K';
    case 76: return 'L';
    case 77: return 'M';
    case 75: return 'N';
    case 78: return 'O';
    case 79: return 'P';
    case 80: return 'Q';
    case 81: return 'R';
    case 82: return 'S';
    case 83: return 'T';
    case 84: return 'U';
    case 85: return 'V';
    case 86: return 'W';
    case 87: return 'X';
    case 88: return 'Y';
    case 89: return 'Z';
    case 97: return 'a';
    case 98: return 'b';
    case 99: return 'c';
    case 100: return 'd';
    case 101: return 'e';
    case 102: return 'f';
    case 103: return 'g';
    case 104: return 'h';
    case 105: return 'i';
    case 106: return 'j';
    case 107: return 'k';
    case 108: return 'l';
    case 109: return 'm';
    case 110: return 'n';
    case 111: return 'o';
    case 112: return 'p';
    case 113: return 'q';
    case 114: return 'r';
    case 115: return 's';
    case 116: return 't';
    case 117: return 'u';
    case 118: return 'v';
    case 119: return 'w';
    case 120: return 'x';
    case 121: return 'y';
    case 122: return 'z';
    case 123: return "{";
    case 124: return "|";
    case 125: return "}";
    case 126: return "~";
  }
}
