# MechaPipi

This is a utility for sharing inputs (ie mouse and keyboard) between multiple computers.

This is being created with local home networks in mind. While this doesn't mean that security isn't important it does mean that security that would be necessary on other types of networks isn't as important here. This is most relevant for the finding other machines part of this.

Requirements for this to be successful:

- It must be able to run on a wide range of hardware. If this can't run on rasbpian on a raspberry pi than I haven't succeeded.
- Easy to use and set up
- Bidirectional, you should be able to use whichever mouse and keyboard you are next to, not have a single server that you have to use as inputs for the other machines.
- Each machine on the network that is running this should be easy to find so you don't have to worry about ip addresses or ports or anything like that.

## How does it work?

Currently this runs in node (although many parts are just node wrappers or C modules)

- ioHook captures io events from the mouse and keyboard (https://www.npmjs.com/package/iohook). This is just a wrapper on libuiohook (a C thing)
- Websockets (the ws module) used to connect computers together and send the io events from one computer to another (https://www.npmjs.com/package/ws) This is native node.
- robotjs is used to recreate the io events from one computer on another.

Currently this has one computer working as the server and the other computer at a client. I intend to change that. There is nothing that would prevent this from being bidirectional.

### How to test

This only works on the local network.

Many keys don't work yet and the mousewheel is ignored. Pretty much it is only some typing, mouse movement, left and right mouse buttons and a middle click that are currently set up.

clone the repo and install on both computers:

```
git clone https://gitlab.com/inmysocks/MechaPipi
cd MechaPipi
npm install
```

On the server computer start the server using

`npm start`

the terminal should say something like this:

```
SERVER
Address  192.168.0.15 : 8000
```

on the other computer start the server using this:

`node ./index.js false 192.168.0.15`

where instead of `192.168.0.15` you put in the ip part of the Address line given by the server (it is the servers ip address).


## Why?

Because I want to. Seriously, if you ask this that is the only answer you will ever get.
